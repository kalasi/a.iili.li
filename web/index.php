<?php

require_once __DIR__ . '/../vendor/autoload.php';

use PHPHtmlParser\Dom;
use Silex\Provider\TwigServiceProvider;
use Symfony\Component\HttpFoundation\Request;

define('ARCHIVES', __DIR__ . '/../storage/archives/');
define('TIMES', __DIR__ . '/../storage/times/');
define('STORAGE', __DIR__ . '/../storage/');

function archive($name, $path) {
    return ARCHIVES . $name . '/' . $path;
}

function curl($url) {
    $ch = curl_init();
    $timeout = 3;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0');
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}
function generateName() {
    $chars = str_split('abcdefghijklmnopqrstuvwxyz'
        . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        . '0123456789');

    // Find a filename.
    do {
        $name = '';
        foreach (array_rand($chars, 4) as $k) $name .= $chars[$k];
    } while (file_exists(ARCHIVES . $name));

    return $name;
}

function getSources($data, $hit, $name) {
    $dom = (new Dom)->load($data);

    $randomId = function () {
        return mt_rand(1, 999999999999999999);
    };

    // Images.
    foreach ($dom->find('img') as $img) {
        $value = $img->getAttribute('src');

        $filename = $randomId();

        $storage = archive($name, $filename);

        $file = fopen($storage, 'w');

        $publicUrl = '/' . $name . '/' . $filename;

        $hitting = $hit;

        if (substr($value, 0, 2) === '//') {
            $hitting = 'http:' . $value;
        } elseif (substr($value, 0, 1) === '/') {
            $hitting .= $value;
        } else {
            $hitting = $value;
        }

        fwrite($file, curl($hitting));
        fclose($file);

        $data = str_replace($value, $publicUrl, $data);
    }

    // Styleheets.
    foreach ($dom->find('link') as $stylesheet) {
        $value = $stylesheet->getAttribute('href');

        // If the link is to root, skip it.
        if ($value === '/') {
            continue;
        }

        $filename = $randomId();

        $storage = archive($name, $filename);

        $publicUrl = '/' . $name . '/' . $filename;

        $hitting = $hit;

        if (substr($value, 0, 1) === '/') {
            $hitting .= $value;
        } else {
            $hitting = $value;
        }

        $contents = curl($hitting);

        $file = fopen($storage, 'w');
        fwrite($file, $contents);
        fclose($file);

        $data = str_replace($value, $publicUrl, $data);
    }

    return $data;
}

$app = new Silex\Application();

$app['debug'] = true;
$app->register(new TwigServiceProvider, [
    'twig.path' => __DIR__ . '/../views',
]);

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.twig');
});

$app->post('go', function (Request $request) use ($app) {
    $requested = $request->get('url');
    $name = $request->get('name');

    if (strlen($requested) < 1) {
        return 'Nothing entered.';
    }

    // Check if the name has already been taken.
    if (strlen($name) > 0 && file_exists(ARCHIVES . $name)) {
        return 'Sorry, that name has already been taken.';
    }

    $parts = parse_url($requested);

    $url = '';

    if (isset($parts['host'])) {
        $url .= $parts['host'];
    }

    $url .= $parts['path'];

    $hit = $url;

    if (isset($parts['query'])) {
        $url .= '?' . $parts['query'];
    }

    $encoded = urlencode($url);

    $data = curl($url);

    if (strlen($name) < 1) {
        $name = generateName();
    }

    mkdir(ARCHIVES . $name);

    $data = getSources($data, $hit, $name);

    $timesPath = TIMES . $encoded;
    $times = [];

    if (file_exists($timesPath)) {
        $times = unserialize(file_get_contents($timesPath));
    }

    array_push($times, [
        'name' => $name,
        'time' => time(),
    ]);

    file_put_contents(ARCHIVES . $name . '/0', $data);
    file_put_contents($timesPath, serialize($times));

    return $app->redirect('/' . $name);
});

$app->get('{name}/{path}', function ($name, $path) use ($app) {
    $search = archive($name, $path);
    if (!file_exists($search)) {
        return 'File not found for: ' . $name;
    }

    $mime = mime_content_type($search);
    echo file_get_contents($search);

    header('Content-Type: ' . $mime);

    die;
})->assert('path', '(.*)');

$app->get('{name}', function ($name) use ($app) {
    $path = ARCHIVES . $name . '/0';

    if (!file_exists($path)) {
        return 'Archive not found.';
    }

    return file_get_contents($path);
})->assert('name', '(.*)');

$app->run();
